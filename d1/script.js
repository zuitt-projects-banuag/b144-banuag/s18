//What is the difference of arrays and object
//Example of array
let grades = [98.5, 94.3, 90.1];

console.log(grades[0]);

// An object is similar to an array. It is a collection of related data and for functionalities. usually it represent real world objects.

let grade = {
	//property or key
	//object initializer/literal notation - object consist of properties, which are used to describe an object. Key value pair use a comma to separate the value.

	math: 89.2 ,
	english: 98.5 ,
	science: 90.1 ,
	filipino: 94.3
}
//We used dot notation to access the properties/value of our object
console.log(grade.english)

//Syntax:
/*
	let objectNAme = {
		keyA: valueA,
		keyB: valueB
	}
*/

let cellphone = {
	brandName: 'Nokia 3310',
	color: 'Dark Blue',
	manufactureDate: 1999
}

console.log(typeof cellphone);

let student = {
	firstName: 'John',
	lastName: 'Smith',
	//Pwedeng magkaroon sa loob ng object ng object
	location: {
		city: 'Tokyo',
		country: 'Japan'
	},
	//Pwede magkaroon ng array sa loob ng object
	emails: ['john@mail.com','johnsmith@mail.xyz'],

	//object method(function inside an object)
	//this. is the owner of the function
	fullName: function(){
		return this.firstName + ' ' + this.lastName;
	}

}
// Para maaccess yung na sa loob ng object sa object
//dot notation
console.log(student.location.city)

//bracket notation
console.log(student['firstName'])

console.log(student.emails)

//Syntax para makuha yung array sa loob ng object
console.log(student.emails[0]);

//Syntax to print the function inside the object
console.log(student.fullName())

//Sample fot 'this'
const person1 = {
	name: 'Jane',
	//In function no need to have a parameter, because gusto lang naman mag hi ng name.
	greeting: function(){
		return 'Hi I\'m ' + this.name
	}
}

console.log(person1.greeting())

//ARRAY OF OBJECTS
//Sample
let contactList = [
	{
		firstName: 'John',
		lastName: 'Smith',
		location: 'Japan'
	},

	{
		firstName: 'Jane',
		lastName: 'Smith',
		location: 'Japan'
	},
	{
		firstName: 'Jasmine',
		lastName: 'Smith',
		location: 'Japan'
	}
]
//Syntax to access the property of John 
console.log(contactList[0].firstName)


let people = [
	{
	name: 'Juanita',
	age: 13
	},
	{
	name: 'Juanito',
	age: 14
	}
]
//Use forEach to access the object inside the array
people.forEach(function(person){
	console.log(person.name)
	})

//Specify the index to console the object
console.log(`${people[0].name} are the list`)

//Creating object using a Constructor Function(JS OBJECT CONSTRUCTOR/OBJECT FROM BLUEPRINTS)
//Create a reusable function to create several objects that have the same data structure
//This is useful for creating multiple copies/instances of an object

//Object Literals
//let object = {}
//Instance - distinct/unique objects
//let object = new object
//An instances is a concrete occurence of any object which emphasizes on the unique identity of it.
/*
Syntax: 
	function ObjectName(keyA, keyB){
		this.keyA = keyA,
		this.keyB = keyB
	}
*/

function Laptop(name, manufactureDate) {
	//The 'this' keywords allows to assign a new object's property by associating them with values received from our parameter
	this.name = name,
	this.manufactureDate = manufactureDate
}

//This is a unique instances of the laptop object
// 'new' to create a instance
//sample
let laptop = new Laptop('Lenovo', 2008)
console.log('Result from creating objects using object constructors')
console.log(laptop)

//This is another unique instance of the laptop object
let myLaptop = new Laptop('Macbook Air', 2020)
console.log('Result from creating object using object constructors')
console.log(myLaptop)

let oldLaptop = Laptop('Portal R2E CCMC', 1980)
console.log('Without new keyword')
console.log(oldLaptop)
// output: undefined
//Kapag wala new keyword ini-invoke nya lang yung function or hindi mag kapag add sa constructors 

//Creating empty objects
let computer = {} //literal
let myComputer = new Object(); //instances

//Print
console.log(myLaptop.name)
console.log(myLaptop['name'])

//Array of object
let array = [laptop, myLaptop] 

console.log(array[0].name)

//iinitializing/Adding/Deleting/reassigning object properties

//initilized/added properties after the objects was created
//this is useful for time when an object's properties are undetermined at the time of creating them

let car = {}

//add an object properties, we can use dot notation
//sample
//dot notation
car.name = 'Honda Civic'
console.log(car)
//bracket notation
car['manufacture date'] = 2019
console.log(car)
//Reassigning object properties(magbabago yung property ng object)
car.name = 'Dodge Charger R/T'
console.log(car)

//Deleting object properties
//Pwede gumamit ng dot notation sa delete
//delete car['manufacture date']
delete car.name
console.log('Result from deleting properties')
console.log(car)

//OBJECT METHODS
//A method is a function which is a property of an object
//They are also functions and one of the key differences they have is that methods are functions related to a specific object

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name)
	}
}

console.log(person)
console.log('Result from object methods')
person.talk()

//Adding methods to object person
person.walk = function (argument) {
	 console.log(this.name + ' walked 25 steps forward')
}
person.walk()

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@mail.xyz'],
	introduce: function(){
		console.log("Hello my name is " + this.firstName + ' ' + this.lastName)
	}
}
friend.introduce()

/*
	Real World Application of Objects
	-Scenario
		1. We would like to create a game that would have several pokemon interact with each other
*/

// Using object literals
let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log('This pokemon tackled target Pokemon')
		console.log("'targetPokemon's health is now reduced to _targetPokemonHealth_")
		
	},
	faint: function(){
		console.log('Pokemon fainted')
	}
} 

//Creating an object constructors instead of object literals.

function Pokemon(name, level){
	this.name = name
	this.level = level
	this.health = 2 * level;
	this.attack = level;

	//Methods
	//skill or technique
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name)
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	};
	this.faint = function(){
		console.log(this.name + ' fainted.')
	}
}

//Instances
let pikachu = new Pokemon('Pikachu', 16)
let charizard = new Pokemon('Charizard', 8)

pikachu.tackle(charizard)